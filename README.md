# Syncer

## INTRODUCTION

Module allows to export and import content in YAML format.

## INSTALLATION

<code>composer require drupal/syncer</code>

## REQUIREMENTS
ZipArchive extension is required.

## CONFIGURATION
The module has no modifiable settings.

## USAGE

```console
drush sce ENTITY_TYPE
drush sci ENTITY_TYPE
```

## DOCUMENTATION

Documentation - https://www.drupal.org/docs/8/modules/syncer

## MAINTEINERS

Lasha Badashvili - https://www.drupal.org/u/lashabp
